import { configureStore } from "@reduxjs/toolkit";

import CreatePassword from "./reducers/create-password";
import CreatePartial from "./reducers/create-partial";
import ListData from "./reducers/list";

const store = configureStore({
  reducer: {
    CreatePassword,
    CreatePartial,
    ListData,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
