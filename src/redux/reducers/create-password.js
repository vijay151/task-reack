import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// utils

import axios from "axios";
import "react-toastify/dist/ReactToastify.css";

import { toast } from "react-toastify";

// tostify
const FAILED = async (data) => {
  toast.error(data, {
    position: toast.POSITION.TOP_RIGHT,
  });
};

const SUCCESS = async (data) => {
  toast.success(data, {
    position: toast.POSITION.TOP_RIGHT,
  });
};

// api call
export const create_password = createAsyncThunk(
  "CreatePassword/create_password",
  async (value, { rejectWithValue }) => {
    try {
      const data = await axios.post(
        `http://localhost:8080/api/password/create`,
        value
      );
      if (data.data.status) {
        return data;
      } else {
        return rejectWithValue(data);
      }
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

const initialState = {
  isLoading: false,
  isSuccess: false,
  response: {},
  Message: "",
};

const slice = createSlice({
  name: "CreatePassword",
  initialState,
  reducers: {
    is_create_success: (state, payload) => {
      state.isSuccess = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(create_password.pending, (state, { payload }) => {
      state.isLoading = true;
    });
    builder.addCase(create_password.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      state.response = payload.data;
      state.Message = payload.data.message;
      state.isSuccess = true;
      SUCCESS(payload.data.message);
    });

    builder.addCase(create_password.rejected, (state, { payload }) => {
      state.isLoading = false;
      state.isSuccess = false;
      FAILED(payload.data.message);
    });
  },
});

// Reducer
export default slice.reducer;

// Actions
export const { is_create_success } = slice.actions;
