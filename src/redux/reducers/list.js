import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// utils

import axios from "axios";
import "react-toastify/dist/ReactToastify.css";

import { toast } from "react-toastify";

// tostify
const FAILED = async (data) => {
  toast.error(data, {
    position: toast.POSITION.TOP_RIGHT,
  });
};

// api call
export const lis_data = createAsyncThunk(
  "ListData/lis_data",
  async (value, { rejectWithValue }) => {
    try {
      const data = await axios.get(`http://localhost:8080/api/list`);
      if (data.data.status) {
        return data;
      } else {
        return rejectWithValue(data);
      }
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

const initialState = {
  isLoading: false,
  isSuccess: false,
  response: {},
  Message: "",
};

const slice = createSlice({
  name: "ListData",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(lis_data.pending, (state, { payload }) => {
      state.isLoading = true;
    });
    builder.addCase(lis_data.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      state.response = payload.data;
      state.Message = payload.data.message;
      state.isSuccess = true;
    });

    builder.addCase(lis_data.rejected, (state, { payload }) => {
      state.isLoading = false;
      state.isSuccess = false;
      FAILED("data list failed");
    });
  },
});

// Reducer
export default slice.reducer;
