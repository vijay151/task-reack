import CreateStrongPassword from "./pages/create-password";
import { Route, Routes, Navigate } from "react-router-dom";
import Partial from "./pages/partial";
import AllData from "./pages/all-data";

function App() {
  return (
    <>
      <Routes>
        <Route exact path="/" element={<CreateStrongPassword />} />
        <Route exact path="/partial" element={<Partial />} />
        <Route exact path="/all-data" element={<AllData />} />
      </Routes>
    </>
  );
}

export default App;
