import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useDispatch, useSelector } from "react-redux";
import { lis_data } from "../redux/reducers/list";
import { useNavigate } from "react-router-dom";

export default function ColumnGroupingTable() {
  const listData = useSelector((state) => state.ListData);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // useEffect
  React.useEffect(() => {
    dispatch(lis_data(""));
  }, []);

  return (
    <Paper sx={{ width: "100%" }}>
      <div className="d-flex ">
        <TableContainer className="px-2" sx={{ maxHeight: "90vh" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="start" colSpan={1}>
                  S.No
                </TableCell>
                <TableCell align="start" colSpan={3}>
                  Password
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {!listData.response.data
                ? "No Passwords Found"
                : listData.response.data.password.map((row, index) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                      >
                        <TableCell align={"start"} colSpan={1}>
                          {index + 1}
                        </TableCell>
                        <TableCell align={"start"}>{row.password}</TableCell>
                      </TableRow>
                    );
                  })}
            </TableBody>
          </Table>
        </TableContainer>
        <TableContainer className="px-2" sx={{ maxHeight: "90vh" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="center" colSpan={3}>
                  Partial Values
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="start" colSpan={1}>
                  S.No
                </TableCell>
                <TableCell
                  align={"center"}
                  style={{ top: 57, minWidth: "100px" }}
                >
                  Array
                </TableCell>
                <TableCell
                  align={"center"}
                  style={{ top: 57, minWidth: "100px" }}
                >
                  Combinations
                </TableCell>
                <TableCell
                  align={"center"}
                  style={{ top: 57, minWidth: "100px" }}
                >
                  Outputs
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {!listData.response.data
                ? "No Partial Found"
                : listData.response.data.partial.map((row, index) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                      >
                        <TableCell align={"start"} colSpan={1}>
                          {index + 1}
                        </TableCell>
                        <TableCell align={"center"}>
                          {JSON.stringify(row.array)}
                        </TableCell>
                        <TableCell align={"center"}>
                          {row.combination}
                        </TableCell>
                        <TableCell align={"center"}>{row.output}</TableCell>
                      </TableRow>
                    );
                  })}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <div
        style={{
          height: "10vh",
        }}
        className="d-flex w-100 justify-content-end px-3 align-items-center"
      >
        <button
          onClick={() => {
            navigate("/partial");
          }}
          className="fw-1-2rem border-0 px-3 ms-5 me-auto py-2 rounded bg-primary text-white "
        >
          Go To Partial
        </button>
        {/* button */}
        <button
          onClick={() => {
            navigate("/");
          }}
          className="fw-1-2rem border-0 me-5 px-3 py-2 rounded bg-primary text-white "
        >
          Go To Password
        </button>
      </div>
    </Paper>
  );
}
