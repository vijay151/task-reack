import "../assets/create-password.css";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useState, useEffect } from "react";
import Switch from "@mui/material/Switch";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  create_partial,
  is_create_success,
} from "../redux/reducers/create-partial";

const Partial = () => {
  const [negative, setNegative] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const createPartial = useSelector((state) => state.CreatePartial);

  // formik initial values
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      data: [],
      number: "",
    },
    onSubmit: (values) => {
      if (values.data.length < 2) {
        alert("2 field required for array");
      } else {
        dispatch(
          create_partial({
            array: values.data,
          })
        );
      }
      //   submit(values);
    },
  });

  //   formik functions
  const {
    values,
    handleBlur,
    handleChange,
    handleSubmit,
    touched,
    setFieldValue,
    setFieldTouched,
    resetForm,
    errors,
  } = formik;

  // useEffect
  useEffect(() => {
    if (createPartial.isSuccess) {
      resetForm();
      dispatch(is_create_success(""));
    }
  }, [createPartial]);

  return (
    <>
      <div className="d-flex justify-content-center align-items-center w-100 vh-100">
        <Paper elevation={3} className="input-card rounded">
          <div className=" d-flex flex-column align-items-center justify-content-center h-100 py-4">
            <h4 className="fw-bold">Partial Two Array</h4>

            {/* check negative number */}
            <div>
              negative number
              <Switch
                checked={negative}
                onChange={() => {
                  setNegative(!negative);
                }}
                inputProps={{ "aria-label": "controlled" }}
              />
            </div>

            {/* input box */}
            <div className="px-4 py-2 px-5 w-100  d-flex  justify-content-center">
              <TextField
                label="Add array of data "
                name="number"
                type="number"
                sx={{
                  height: "40px",
                }}
                className="w-100 py-1 mx-2"
                variant="standard"
                value={values.number}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <button
                onClick={() => {
                  if (values.number) {
                    setFieldValue("data", [
                      ...values.data,
                      negative
                        ? "-" + Number(values.number)
                        : Number(values.number),
                    ]);
                    setFieldValue("number", "");
                  }
                }}
                className="fw-1-2rem border-0 px-3 py-1 mt-3 mx-2 rounded bg-primary text-white"
                style={{
                  height: "40px",
                }}
              >
                Add
              </button>
            </div>
            <div className="d-flex  h-100 align-items-center fs-20 fw-401">
              <div>Array=[ {values.data.map((e) => e + ", ")}]</div>
            </div>
            {/* reset array */}
            <div className="d-flex w-100 justify-content-end px-3">
              <button
                onClick={() => {
                  setFieldValue("data", []);
                  resetForm();
                }}
                className="fw-1-2rem border-0 px-3 ms-5 me-auto py-2 rounded bg-primary text-white "
              >
                Reset Array
              </button>
              {/* button */}
              <button
                onClick={() => {
                  if (!createPartial.isLoading) {
                    handleSubmit();
                  }
                }}
                className="fw-1-2rem border-0 me-5 px-3 py-2 rounded bg-primary text-white "
              >
                {createPartial.isLoading ? "Loading..." : " Submit"}
              </button>
            </div>
            {/* view all passwords */}
            <div className="d-flex w-100 justify-content-end">
              <p
                className="p-3 mb-0 me-auto view-password"
                onClick={() => {
                  navigate("/");
                }}
              >
                password task
              </p>
              <p
                className="p-3 mb-0 view-password"
                onClick={() => {
                  navigate("/all-data");
                }}
              >
                View partial data
              </p>
            </div>
          </div>
        </Paper>
      </div>
    </>
  );
};

export default Partial;
