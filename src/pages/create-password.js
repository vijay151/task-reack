import "../assets/create-password.css";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import Switch from "@mui/material/Switch";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  create_password,
  is_create_success,
} from "../redux/reducers/create-password";

const CreateStrongPassword = () => {
  const [numOfError, setNumOfError] = useState(0);
  const [minLetter, setMinLetter] = useState(true);
  const createPassword = useSelector((state) => state.CreatePassword);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // validation scheemas
  const SignInSchema = Yup.object().shape({
    password: Yup.string()
      .min(6, "password must 6 character above")
      .max(20, "Password must not exceed 20 characters")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.{8,})/,
        // : /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
        "Must Contain 6 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
      )
      .test("password", "week password", (value) => {
        if (/(.)\1\1/.test(value)) {
          return false;
        } else {
          return true;
        }
      }),
  });

  // formik initial values
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      password: "",
    },
    validationSchema: SignInSchema,
    onSubmit: (values) => {
      dispatch(create_password(values));
    },
  });

  //   formik functions
  const {
    values,
    handleBlur,
    handleChange,
    handleSubmit,
    touched,
    setFieldValue,
    setFieldTouched,
    resetForm,
    errors,
  } = formik;

  // useEffect
  useEffect(() => {
    if (createPassword.isSuccess) {
      resetForm();
      dispatch(is_create_success(""));
    }
  }, [createPassword]);

  // get pending numbers

  const check = (e) => {
    let value = 5;

    const password = e.target.value;
    // check number

    if (password.length > 5 && password.length <= 20) {
      value = value - 1;
    }
    //  check number
    const number = /^(?=.*[0-9])/;
    if (number.test(password)) {
      value = value - 1;
    }
    // check lowercase

    const lowercase = /^(?=.*[a-z])/;
    if (lowercase.test(password)) {
      value = value - 1;
    }
    // check uppercase

    const uppercase = /^(?=.*[A-Z])/;
    if (uppercase.test(password)) {
      value = value - 1;
    }
    //  check special character
    // let format = /^(?=.*[!@#\$%\^&\*])/;
    // if (format.test(password)) {
    //   value = value - 1;
    // }

    // check repeated sentance
    if (!/(.)\1\1/.test(password)) {
      value = value - 1;
    }
    setNumOfError(value);
  };

  console.log(6 - values.password.length);
  return (
    <>
      <div className="d-flex justify-content-center align-items-center w-100 vh-100">
        <Paper elevation={3} className="input-card rounded">
          <div className=" d-flex flex-column align-items-center justify-content-center h-100 py-4">
            <h4 className="fw-bold">Create Strong Password</h4>
            <div>
              Min steps for create strong password
              <Switch
                checked={!minLetter}
                onChange={() => {
                  setMinLetter(!minLetter);
                }}
                inputProps={{ "aria-label": "controlled" }}
              />
            </div>

            {/* conditions */}
            <div className="px-4 pt-3 ms-3">
              <div
                style={{
                  fontWeight: "600",
                }}
              >
                conditions for make strong password
              </div>
              <ul>
                <li>Password must min 6 and max 20 characters</li>
                <li>Include One Uppercase, One Lowercase and One Number</li>
                <li>Don't Repeat 3 character continuously</li>
              </ul>
            </div>
            {/* input box */}
            <div className="px-4 py-2 px-5 w-100 h-100 d-flex flex-column justify-content-center">
              <TextField
                label="Password "
                name="password"
                type="password"
                className="w-100 py-1"
                error={
                  minLetter
                    ? values.password.length >= 6
                      ? touched.password && errors.password
                        ? true
                        : false
                      : true
                    : numOfError === 0
                    ? false
                    : true
                }
                variant="standard"
                helperText={
                  minLetter
                    ? 6 - values.password.length <= 0
                      ? touched.password
                        ? errors.password
                        : ""
                      : 6 - values.password.length + " character is required "
                    : numOfError === 0
                    ? ""
                    : numOfError + " steps required for make strong password"
                }
                onChange={(e) => {
                  setFieldValue("password", e.target.value);
                  check(e);
                }}
                onBlur={handleBlur}
                value={values.password}
              />
            </div>
            {/* button */}
            <button
              onClick={() => {
                if (!createPassword.isLoading) {
                  handleSubmit();
                }
              }}
              className="fw-1-2rem border-0 px-3 py-2 rounded bg-primary text-white "
            >
              {createPassword.isLoading ? "Loading..." : " Submit"}
            </button>
            {/* view all passwords */}
            <div className="d-flex w-100 justify-content-end">
              <p
                className="p-3 mb-0 me-auto view-password"
                onClick={() => {
                  navigate("/partial");
                }}
              >
                partial array task
              </p>
              <p
                className="p-3 mb-0 view-password"
                onClick={() => {
                  navigate("/all-data");
                }}
              >
                {" "}
                View all data
              </p>
            </div>
          </div>
        </Paper>
      </div>
    </>
  );
};

export default CreateStrongPassword;
